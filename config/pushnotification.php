<?php

return [

    'iOS' => [
        'environment' => env('IOS_PUSH_ENV', 'development'),
        'certificate' => storage_path('certificates') . DIRECTORY_SEPARATOR . env('IOS_PUSH_CERT', ''),
        'passPhrase'  => env('IOS_PUSH_PASSWORD', ''),
        'service'     => 'apns'
    ],

    'android' => [
        'environment' => env('ANDROID_PUSH_ENV', 'development'),
        'apiKey'      => env('ANDROID_PUSH_API_KEY', 'yourAPIKey'),
        'service'     => 'gcm'
    ]

];
