<?php

namespace App\Providers;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Builder::macro('orderByRandom', function () {
            $randomFunctions = [
                'mysql'  => 'RAND()',
                'pgsql'  => 'RANDOM()',
                'sqlite' => 'RANDOM()',
                'sqlsrv' => 'NEWID()',
            ];

            $driver = $this->getConnection()->getDriverName();
            return $this->orderByRaw($randomFunctions[$driver]);
        });

        DB::enableQueryLog();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
