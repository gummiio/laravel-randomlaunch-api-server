<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers\Web';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapWebRoutes($router);

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        $router->group([
            'namespace'  => $this->namespace,
            'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes.php');
        });

        $router->group([
            'namespace'  => 'App\Http\Controllers\Api',
            'middleware' => ['web', 'api'],
            'prefix'     => 'api/v1',
            'as'         => 'api::'
        ], function ($router) {
            \Debugbar::disable();
            require app_path('Http/apiRoutes.php');
        });

        $router->group([
            'namespace'  => 'App\Http\Controllers\Admin',
            'middleware' => ['web', 'api.admin'],
            'prefix'     => 'admin/api/v1',
            'as'         => 'admin::'
        ], function ($router) {
            \Debugbar::disable();
            require app_path('Http/adminRoutes.php');
        });
    }
}
