<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // Api
        \Randomlaunch\Api\Events\ApiTokenLimitExceeded::class => [
            \Randomlaunch\Api\Listeners\ApiTokenLimitExceeded\NotifyOnSlack::class,
        ],
        \Randomlaunch\Api\Events\LongApiRespondTimeExecuted::class => [
            \Randomlaunch\Api\Listeners\LongApiRespondTimeExecuted\NotifyOnSlack::class,
        ],

        // Link
        \Randomlaunch\Link\Events\LinkCreated::class => [
            \Randomlaunch\Link\Listeners\LinkCreated\GenerateDeviceScreenshots::class,
        ],
        \Randomlaunch\Link\Events\LinkScreenshotGenerationCompleted::class => [
            \Randomlaunch\Link\Listeners\LinkScreenshotGenerationCompleted\createLinkScreenshotMedias::class,
        ],
        \Randomlaunch\Link\Events\LinkScreenshotGenerationFailed::class => [
            \Randomlaunch\Link\Listeners\LinkScreenshotGenerationFailed\UpdateDefaultScreenshot::class,
        ],
        \Randomlaunch\Link\Events\LinkDeleted::class => [
            \Randomlaunch\Link\Listeners\LinkDeleted\RemoveLinkScreenshots::class,
        ],

        // Launch
        \Randomlaunch\Link\Events\LaunchedLinkNotFound::class => [
            \Randomlaunch\Link\Listeners\LaunchedLinkNotFound\RegenerateLaunch::class,
        ],
        \Randomlaunch\Link\Events\LaunchedLinkWasLoaded::class => [
            \Randomlaunch\Link\Listeners\LaunchedLinkWasLoaded\RecordLaunchStatistic::class,
        ],

        // Media
        \Randomlaunch\Media\Events\MediaCreated::class => [
            \Randomlaunch\Media\Listeners\MediaCreated\UploadImageToCdn::class
        ],

        // Page
        \Randomlaunch\Page\Events\PageNotFound::class => [
            \Randomlaunch\Page\Listeners\PageNotFound\NotifyOnSlack::class
        ],

        // Access
        \Randomlaunch\Access\Events\UserCreated::class => [
            \Randomlaunch\Access\Listeners\UserCreated\NotifyOnSlack::class
        ],

    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
