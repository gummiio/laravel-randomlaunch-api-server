<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Randomlaunch\Exceptions\ApiResponseException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,

        ApiResponseException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof MethodNotAllowedHttpException) {
            return response()->apiError('Method not allowed.', [
                //
            ], 405);
        }

        if ($e instanceof ModelNotFoundException) {
            return response()->apiError('Resource not found.', [
                'type'    => 'ResourceNotFound',
                'code'    => 'NFON-001',
                'message' => 'Unable to find the resource you are looking for.'
            ], 404);
        }

        if ($e instanceof ApiResponseException) {
            return response()->apiError($e->getMessage(), $e->getErrorObject(), $e->getCode(), $e->getExtras());
        }

        return parent::render($request, $e);
    }
}
