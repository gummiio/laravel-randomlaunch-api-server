<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        // $schedule->call(function () {
        //     \Illuminate\Support\Facades\Log::info('croned');
        // })->everyMinute();

        // $schedule->call(function () {
        //     (new \Randomlaunch\Link\Helper\KeywordGenerator)->generate()->make();
        // })->twiceDaily(1, 13);

        $schedule->call(function () {
            (new \Randomlaunch\Link\Helper\LinkGenerator)->generate()->makeLinks();
        })->twiceDaily(1, 13);

        $schedule->call(function () {
            (new \Randomlaunch\Link\Helper\LinkGenerator)->generate()->makeLinks();
        })->twiceDaily(7, 19);

        $schedule->call(function () {
            $launcher = new \Randomlaunch\Link\Helper\LinkLauncher;
            if (! $launcher->needToLaunch()) return;

            $launcher->choose()->launch();
        })->hourly();
    }
}
