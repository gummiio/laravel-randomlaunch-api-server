<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Randomlaunch\Link\Events\LaunchedLinkWasLoaded;
use Randomlaunch\Link\Helper\LinkLauncher;
use Randomlaunch\Link\Model\Link;

class PageController extends Controller
{
    public function launch(Request $request)
    {
        if (! $link = Cache::get('currentLaunch')) {
            if (! $link = Link::getCurrentLaunch()) {
                return $this->show404($request);
            }

            Cache::put('currentLaunch', $link, (new LinkLauncher)->nextLaunchTime()->subSecond());
        }

        event(new LaunchedLinkWasLoaded($link, $request));

        return redirect($link->effective_url);
    }
}
