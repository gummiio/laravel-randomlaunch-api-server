<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Randomlaunch\Link\Helper\LinkLauncher;

class LaunchController extends ApiController
{
    public function countDown()
    {
        if (! $nextLaunchTime = Cache::get('nextLaunchTime')) {
            $nextLaunchTime = (new LinkLauncher)->nextLaunchTime();
            Cache::put('nextLaunchTime', $nextLaunchTime, $nextLaunchTime->subSecond());
        }

        return Response()->apiSuccess('', [
            'countDown' => $nextLaunchTime->diffInSeconds(Carbon::now()),
            'nextTimestamp' => $nextLaunchTime->timestamp
        ]);
    }
}
