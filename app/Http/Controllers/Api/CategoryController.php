<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Randomlaunch\Link\Model\Category;

class CategoryController extends ApiController
{
    public function index()
    {
        return Response()->apiSuccess('', Category::all());
    }

    public function show(Category $categories)
    {
        return $categories;
    }
}
