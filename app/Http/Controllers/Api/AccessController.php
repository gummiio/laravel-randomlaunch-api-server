<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Randomlaunch\Access\Events\UserCreated;
use Randomlaunch\Access\Model\User;
use Randomlaunch\Access\Requests\UserRegisterRequest;

class AccessController extends ApiController
{
    public function register(UserRegisterRequest $request)
    {
        if ($user = User::where('device_unique_id', $request->input('device_unique_id'))->first()) {
            return response()->apiSuccess('User already exists.', $user);
        }

        $user = new User($request->all());
        $user->username = snake_case(camel_case($request->input('device_unique_id')));
        $user->password = bcrypt(microtime(true) . $request->input('device_unique_id'));
        $user->api_token = sha1($user->username . str_random(20));
        $user->save();

        event(new UserCreated($user));

        return response()->apiSuccess('User Created.', $user, 201);
    }

    public function login(Request $request)
    {
        $user = auth()->user();
        $user->last_login = Carbon::now();
        $user->save();

        return response()->apiSuccess('User logged in.', $user);
    }
}
