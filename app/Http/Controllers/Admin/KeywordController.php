<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Randomlaunch\Link\Model\Keyword;

class KeywordController extends AdminController
{
    public function index()
    {
        return Keyword::all();
    }

    public function store(Request $request)
    {
        return Keyword::create($request->all());
    }

    public function show(Keyword $keywords)
    {
        return $keywords;
    }

    public function update(Keyword $keywords, Request $request)
    {
        $keywords->update($request->all());
        return $keywords;
    }

    public function destroy(Keyword $keywords)
    {
        $keywords->delete();

        return '';
    }
}
