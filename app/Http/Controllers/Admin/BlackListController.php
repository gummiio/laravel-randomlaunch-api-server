<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Randomlaunch\Link\Model\Blacklist;

class BlackListController extends AdminController
{
    public function index()
    {
        return Blacklist::all();
    }

    public function store(Request $request)
    {
        return Blacklist::create($request->all());
    }

    public function show(Blacklist $blacklists)
    {
        return $blacklists;
    }

    public function update(Blacklist $blacklists, Request $request)
    {
        $blacklists->update($request->all());
        return $blacklists;
    }

    public function destroy(Blacklist $blacklists)
    {
        $blacklists->delete();

        return [];
    }
}
