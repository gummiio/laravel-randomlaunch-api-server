<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Randomlaunch\Link\Model\Category;

class CategoryController extends AdminController
{
    public function index()
    {
        return Category::all();
    }

    public function store(Request $request)
    {
        return Category::create($request->all());
    }

    public function show(Category $categories)
    {
        return $categories;
    }

    public function update(Category $categories, Request $request)
    {
        $categories->update($request->all());
        return $categories;
    }

    public function destroy(Category $categories)
    {
        $categories->delete();

        return '';
    }
}
