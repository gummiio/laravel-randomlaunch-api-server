<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use Auth;
use Illuminate\Http\Request;
use Randomlaunch\Access\Repository\UserRepository;
use Randomlaunch\Access\Requests\UserRegisterRequest;
use Validator;


class AccessController extends ApiController
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth:app', [
            'only' => ['logout']
        ]);

        $this->request = $request;
    }

    public function login()
    {
        if (! $user = Auth::user()) {
            $validator = Validator::make($this->request->all(), [
                'username' => 'required',
                'password' => 'required',
            ]);

            if ($validator->fails()) {
                throw new ApiValicationException($validator);
            }

            if (! $user = UserRepository::getUserFromRequest($this->request)) {
                throw new ApiResponseException('Login failed.', [
                    'type'    => 'WrongCrerdential',
                    'code'    => 'AUTH-004',
                    'message' => 'Providedu username or password don\'t match'
                ], 401);
            }
        }

        return response()->apiSuccess('Logged in.', $user);
    }

    public function register(UserRegisterRequest $request)
    {

    }
}

/**
 * POST /login
 * POST /logout
 * POST /register
 * POST /password_reset
 */
