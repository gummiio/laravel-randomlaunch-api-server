<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Randomlaunch\Page\Model\Page;

class PageController extends AdminController
{
    public function index()
    {
        return Page::all();
    }

    public function store(Request $request)
    {
        return Page::create($request->all());
    }

    public function show(Page $pages)
    {
        return $pages;
    }

    public function update(Page $pages, Request $request)
    {
        $pages->update($request->all());
        return $pages;
    }

    public function destroy(Page $pages)
    {
        $pages->delete();

        return [];
    }
}
