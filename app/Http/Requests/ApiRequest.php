<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Randomlaunch\Exceptions\ApiForbiddenException;
use Randomlaunch\Exceptions\ApiValidationException;

abstract class ApiRequest extends Request
{
    /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws \Illuminate\Http\Exception\HttpResponseException
     */
    protected function failedAuthorization()
    {
        throw new ApiForbiddenException();
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return mixed
     */
    protected function failedValidation(Validator $validator)
    {
       throw new ApiValidationException($validator);
    }
}
