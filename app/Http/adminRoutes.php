<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for adminitration pages.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/logs', ['middleware' => 'auth:admin', function () {
    return Randomlaunch\Api\Model\ApiLog::orderBy('created_at', 'desc')->take(10)->get();
}]);

Route::group(['middleware' => 'auth:admin'], function() {

    Route::resource('pages', 'PageController', [
        'except' => ['create', 'edit']
    ]);

    Route::resource('categories', 'CategoryController', [
        'except' => ['create', 'edit']
    ]);

    Route::resource('keywords', 'KeywordController', [
        'except' => ['create', 'edit']
    ]);

    Route::resource('blacklists', 'BlackListController', [
        'except' => ['create', 'edit']
    ]);

    // Route::resource('links', 'LinkController', [
    //     'except' => ['create', 'edit']
    // ]);

    // Route::group(['prefix' => 'links'], function() {
    //     Route::post('{link}/approved', 'LinkController@approved');
    //     Route::post('{link}/denied', 'LinkController@denied');
    // });

});
