<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('launch', 'PageController@launch');

Route::get('/', function () {
    \Debugbar::disable();
    return View('coming-soon');
});

Route::get('debug', function () {
    // (new \Randomlaunch\Link\Helper\KeywordGenerator)->generate()->make();

    $launcher = new \Randomlaunch\Link\Helper\LinkLauncher;
    if (! $launcher->needToLaunch() && ! Request()->has('forced')) return;

    $launcher->choose()->launch();
    dump($launcher);

    return ':)';
});

Route::get('push_test/{token?}', function ($token = null) {
    if (! $token) return 'need you token';

    $push = \PushNotification::app('iOS')
        ->to($token)
        ->send('Hello World, i`m a push message');

    dump($push);

    return ':)';
});
