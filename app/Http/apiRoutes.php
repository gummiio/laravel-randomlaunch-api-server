<?php


Route::get('/', ['middleware' => 'auth:app', function () {
    return Response::apiSuccess("Yeeep app", [], 200);
}]);

Route::get('/long', ['middleware' => 'auth:app', function () {
    sleep(6);
    return Response::apiSuccess("yerpp", [], 200);
}]);

/*
|--------------------------------------------------------------------------
| API Routes, No Auth Required
|--------------------------------------------------------------------------
*/
Route::post('register', 'AccessController@register');

/*
|--------------------------------------------------------------------------
| API Routes, Auth Required
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'auth:app'], function() {

    Route::post('login', 'AccessController@login');

    Route::resource('pages', 'PageController', [
        'only' => ['show']
    ]);

    Route::resource('categories', 'CategoryController', [
        'only' => ['index', 'show']
    ]);

    Route::get('launch/countdown', 'LaunchController@countDown');

});
