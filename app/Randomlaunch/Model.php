<?php

namespace Randomlaunch;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Facades\Route;
use Randomlaunch\Api\Transformer\ApiTransformer;
use Randomlaunch\Api\Transformer\BaseTransformer;

class Model extends EloquentModel
{
    protected $apiTransformer = ApiTransformer::class;

    protected function isApiRequest()
    {
        return strpos(Route::current()->getName(), 'api::') === 0;
    }

    public function is(EloquentModel $model)
    {
        return ($this instanceof $model) && ($this->getKey() == $model->getKey());
    }

    public function attributesToArray()
    {
        $attributes = parent::attributesToArray();

        if (property_exists($this, 'apiTransformer') && $this->apiTransformer) {
            return (new $this->apiTransformer($attributes))->transform();
        }

        return $attributes;
    }
}
