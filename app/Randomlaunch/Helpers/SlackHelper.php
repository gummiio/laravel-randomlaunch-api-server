<?php

namespace Randomlaunch\Helpers;

use Maknz\Slack\Facades\Slack;

class SlackHelper
{
    /**
     * Message body
     *
     * @var string
     */
    protected $text = '';

    /**
     * Attachment color
     *
     * @var string
     */
    protected $color = 'good';

    /**
     * Attachment fields
     *
     * @var array
     */
    protected $fields = [];

    /**
     * Initilize a new instance
     *
     * @return object
     */
    public static function new()
    {
        return new static();
    }

    /**
     * Set the message body
     *
     * @param  string $text message body
     * @return object
     */
    public function text($text = '')
    {
        $this->text = $text
            ? $text . PHP_EOL . str_repeat('=', strlen($text))
            : '';

        return $this;
    }

    /**
     * Set the attachment Color
     *
     * @param  [type] $color hax code or string
     * @return object
     */
    public function color($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Add a field
     *
     * @param string  $content field content
     * @param string  $title   field title
     * @param boolean $short   field width
     */
    public function addField($content = '', $title = '', $short = false)
    {
        $field = [
            'title' => $title,
            'value' => is_array($content)? implode(PHP_EOL, $content) : $content,
            'short' => $short
        ];

        $this->fields[] = $field;

        return $this;
    }

    /**
     * Build the slack object
     *
     * @return object
     */
    public function build()
    {
        return Slack::attach([
            'text' => $this->text,
            'color' => $this->color,
            'fields' => $this->fields
        ]);
    }

    /**
     * Send the slack message
     *
     * @return void
     */
    public function send()
    {
        $this->build()->send();
    }

    /**
     * Send the spesific slack channel
     */
    public function sendTo($channel)
    {
        $this->build()->to($channel)->send();
    }

}
