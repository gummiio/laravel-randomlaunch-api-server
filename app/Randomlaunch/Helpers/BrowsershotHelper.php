<?php

namespace Randomlaunch\Helpers;

use Spatie\Browsershot\Browsershot;

class BrowsershotHelper
{
    protected $browsershot;

    protected $namedSizes = [
        'mobile'  => [320, 480],
        'tablet'  => [768, 1024],
        'desktop' => [1024, 768]
    ];

    protected $webUrl;

    protected $background = 'white';

    protected $timeout = 5000;

    protected $target = 5000;

    protected $quality = 80;

    protected $fullpage = false;

    protected $format = 'jpg';

    private function __construct($url = '')
    {
        $this->browsershot = new Browsershot;
        $this->webUrl($url);
        $this->target();
    }

    public static function init($url = '')
    {
        return new static($url);
    }

    public function webUrl($url)
    {
        $this->webUrl = $url;

        return $this;
    }

    public function background($color = 'white')
    {
        $this->background = $color;

        return $this;
    }

    public function timeout($timeout = 5000)
    {
        $this->timeout = $timeout;

        return $this;
    }

    public function target($target = 'browsershot')
    {
        $this->target = storage_path($target) . DIRECTORY_SEPARATOR;

        return $this;
    }

    public function quality($quality = 80)
    {
        $this->quality = $quality;

        return $this;
    }

    public function fullpage($bool = false)
    {
        $this->fullpage = $bool;

        return $this;
    }

    public function format($format = 'jpg')
    {
        $this->format = in_array($format, ['jpg', 'jpeg', 'png'])? $format : 'jpg';

        return $this;
    }

    public function generateFileName($name = '')
    {
        if (! $name) {
            $name = parse_url($this->webUrl, PHP_URL_HOST);
        }

        $name = explode('?', $name);

        $name = array_shift($name);

        return str_slug(str_replace('.', '-', $name));
    }

    public function take($name = null, $width = 500, $height = 500)
    {
        if (! $this->webUrl) return false;

        $this->setup();

        $save = $this->browsershot
            ->setWidth($width)
            ->setHeight($height)
            ->save($this->getTargetLocation($name));

        return $save? $this->getTargetLocation($name) : false;
    }

    protected function setup()
    {
        $this->browsershot
            ->setUrl($this->webUrl)
            ->setTimeout($this->timeout)
            ->setQuality($this->quality)
            ->setBackgroundColor($this->background);

        if ($this->fullpage) {
            $this->browsershot->setHeightToRenderWholePage();
        }
    }

    protected function getTargetLocation($name = '')
    {
        $name = $this->generateFileName($name);

        return "{$this->target}{$name}.{$this->format}";
    }

    public function __call($method, $args)
    {
        if (strpos($method, 'take') === 0 && array_key_exists(($key = strtolower(str_replace('take', '', $method))), $this->namedSizes)) {
            $name = isset($args[0])? $args[0] : '';
            $width = $this->namedSizes[strtolower($key)][0];
            $height = $this->namedSizes[strtolower($key)][1];

            return $this->take($name, $width, $height);
        }
    }
}
