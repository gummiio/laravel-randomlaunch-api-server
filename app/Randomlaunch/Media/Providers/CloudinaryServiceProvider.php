<?php

namespace Randomlaunch\Media\Providers;

use Cloudinary;
use Illuminate\Support\ServiceProvider;
use Randomlaunch\Media\Events\MediaCreated;
use Randomlaunch\Media\Model\Media;

class CloudinaryServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        Media::created(function ($media) {
            event(new MediaCreated($media));
        });

        // Media::deleted(function ($media) {
        //     event(new MediaDeleted($media->id));
        // });
    }

    public function register()
    {

    }
}
