<?php

namespace Randomlaunch\Media\Listeners\MediaCreated;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Randomlaunch\Helpers\CloudinaryHelper;
use Randomlaunch\Media\Events\MediaCreated;

class UploadImageToCdn implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LinkDeleted  $event
     * @return void
     */
    public function handle(MediaCreated $event)
    {
        $media = $event->media;

        try {
            $upload = CloudinaryHelper::init($media->getPath(), $media->cloud_id)
                ->upload('screenshots/' . $media->id . '--' . $media->filename);

            $media->cdn_id = $upload['public_id'];
            $media->cdn_url = $upload['secure_url'];
            $media->cnd_info = json_encode($upload);

            $media->save();

            $media->removeLocalCopy();
        } catch(\Exception $e) {
            // TODO
        }
    }
}
