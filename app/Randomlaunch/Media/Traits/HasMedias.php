<?php

namespace Randomlaunch\Media\Traits;

use Illuminate\Database\Eloquent\Model;
use Randomlaunch\Media\Model\Media;

trait HasMedias
{
    public static function bootHasMedias()
    {
        static::deleted(function (Model $model) {
            $model->medias()->get()->each(function (Media $media) use ($model) {
                if ($model->forceDeleting) {
                    $media->forceDelete();
                } else {
                    $media->delete();
                }
            });
        });

        static::restored(function (Model $model) {
            $model->medias()->withTrashed()->get()->each(function (Media $media) {
                $media->restore();
            });
        });
    }

    public function medias()
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    public function getMediaById($id)
    {
        return $this->medias->first(function ($index, $media) use ($id) {
            return $media->id == $id;
        });
    }
}
