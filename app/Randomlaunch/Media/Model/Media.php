<?php

namespace Randomlaunch\Media\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Randomlaunch\Access\Model\User;
use Randomlaunch\Model;

class Media extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'medias';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'filename', 'extension', 'mime_type',
        'dir_path', 'cloud_id', 'file_size', 'metadata'
    ];

    /**
     * Get the upload user.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get all of the owning mediable models.
     */
    public function mediable()
    {
        return $this->morphTo();
    }

    public function getPath()
    {
        return $this->dir_path . DIRECTORY_SEPARATOR . $this->filename . '.' . $this->extension;
    }

    public function removeLocalCopy()
    {
        $path = $this->getPath();
        return @unlink($path);
    }
}
