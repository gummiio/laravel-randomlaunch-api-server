<?php

namespace Randomlaunch\Media\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Randomlaunch\Media\Model\Media;

class MediaCreated extends Event
{
    use SerializesModels;

    /**
     * Keyword Object
     *
     * @var object
     */
    public $media;

    /**
     * Create a new event instance.
     *
     * @param Keyword $media Keyword Object
     * @return void
     */
    public function __construct(Media $media)
    {
        $this->media = $media;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
