<?php

namespace Randomlaunch\Moderation\Flaggable;

use Randomlaunch\Moderation\Flaggable\FlaggableScope;

trait Flaggable
{
    /**
     * Which scope is associated with this trait
     *
     * @var string
     */
    protected static $flaggableScopeClass = flaggableScope::class;

    /**
     * Indicates if the model needs to apply the scope automatically.
     *
     * @var boolean
     */
    protected $applyFlaggableScope = false;

    /**
     * Indicate which macro the attach on global scope.
     *
     * @var boolean
     */
    protected $defaultFlaggableMacro = 'withoutFlagged';

    /**
     * Indicate which macro the remove on global scope.
     *
     * @var boolean
     */
    protected $removeDefaultFlaggableMacro = 'withFlagged';

    /**
     * Boot the flaggable trait for a model.
     *
     * @return void
     */
    public static function bootFlaggable()
    {
        static::addGlobalScope(new static::$flaggableScopeClass);
    }

    /**
     * Get the name of the column for applying the scope.
     *
     * @return string
     */
    public function getFlaggedColumn()
    {
        return defined('static::FLAGGED_COLUMN') ? static::FLAGGED_COLUMN : 'flagged_at';
    }

    /**
     * Get the fully qualified column name for applying the scope.
     *
     * @return string
     */
    public function getQualifiedFlaggedColumn()
    {
        return $this->getTable().'.'.$this->getFlaggedColumn();
    }

    /**
     * Determine if the model instance has been flagged.
     *
     * @return boolean
     */
    public function isFlagged()
    {
        return ! is_null($this->{$this->getFlaggedColumn()});
    }

    /**
     * Flag this model instance.
     *
     * @return bool|null
     */
    public function flag()
    {
        $this->{$this->getFlaggedColumn()} = $this->freshTimestamp();
        return $this->save();
    }

    /**
     * Unflag this model instance.
     *
     * @return bool|null
     */
    public function unflag()
    {
        $this->{$this->getFlaggedColumn()} = null;
        return $this->save();
    }

    /**
     * Get a new query builder that excludes flagged results.
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public static function withoutFlagged()
    {
        return (new static)->newQueryWithoutScope(new static::$flaggableScopeClass)
            ->withoutFlagged();
    }

    /**
     * Get a new query builder that includes flagged results.
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public static function withFlagged()
    {
        return (new static)->newQueryWithoutScope(new static::$flaggableScopeClass)
            ->withFlagged();
    }

    /**
     * Get a new query builder that only includes flagged results.
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public static function onlyFlagged()
    {
        return (new static)->newQueryWithoutScope(new static::$flaggableScopeClass)
            ->onlyFlagged();
    }

    /**
     * Check if auto apply global scope is set on this model instance.
     *
     * @return boolean
     */
    public function applyFlaggableScope()
    {
        return $this->applyFlaggableScope;
    }

    /**
     * Get the default macro of this trait
     *
     * @return string
     */
    public function defaultFlaggableMacro()
    {
        return $this->defaultFlaggableMacro;
    }

    /**
     * Get the default remove macro of this trait
     *
     * @return string
     */
    public function removeDefaultFlaggableMacro()
    {
        return $this->removeDefaultFlaggableMacro;
    }


}
