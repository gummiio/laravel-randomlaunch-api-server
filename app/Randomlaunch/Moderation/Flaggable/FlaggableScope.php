<?php

namespace Randomlaunch\Moderation\Flaggable;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ScopeInterface;

class FlaggableScope implements ScopeInterface
{
    /**
     * All of the extensions to be added to the builder.
     *
     * @var array
     */
    protected $extensions = ['WithFlagged', 'WithoutFlagged', 'OnlyFlagged'];

    /**
     * Apply scope on the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder  $builder
     * @param \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        // Apply the extensions, so we can use it early
        $this->extend($builder);

        // in case of turning this scope off manually, don't need to apply
        if ($model->applyFlaggableScope() && $model->defaultFlaggableMacro()) {
            $builder->{$model->defaultFlaggableMacro()}();
        }
    }

    /**
     * Remove scope from the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder  $builder
     * @param \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function remove(Builder $builder, Model $model)
    {
        if ($model->removeDefaultFlaggableMacro()) {
            $builder->{$model->removeDefaultFlaggableMacro()}();
        }
    }

    /**
     * Extend the query builder with the needed functions.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    public function extend(Builder $builder)
    {
        foreach ($this->extensions as $extension) {
            $this->{"add{$extension}"}($builder);
        }
    }

    /**
     * Add the with-flagged extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addWithFlagged(Builder $builder)
    {
        $builder->macro('withFlagged', function (Builder $builder) {

            $column = $this->extractModelColumn($builder);

            $query = $builder->getQuery();

            $query->wheres = collect($query->wheres)
                ->reject(function ($where) use ($column) {
                    return $where['column'] == $column;
                }
            )->values()->all();

            return $builder;
        });
    }

    /**
     * Add the without-flagged extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addWithoutFlagged(Builder $builder)
    {
        $builder->macro('withoutFlagged', function (Builder $builder) {

            $column = $this->extractModelColumn($builder);

            $builder->whereNull($column);

            return $builder;
        });
    }

    /**
     * Add the only-flagged extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addOnlyFlagged(Builder $builder)
    {
        $builder->macro('onlyFlagged', function (Builder $builder) {

            $column = $this->extractModelColumn($builder);

            $builder->getQuery()->whereNotNull($column);

            return $builder;
        });
    }

    /**
     * Extract the qualified collumn name from builder
     *
     * @param  Builder $builder
     * @return string
     */
    protected function extractModelColumn(Builder $builder)
    {
        return $builder->getModel()->getQualifiedFlaggedColumn();
    }

}
