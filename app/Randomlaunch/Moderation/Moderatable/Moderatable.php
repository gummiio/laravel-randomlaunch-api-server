<?php

namespace Randomlaunch\Moderation\Moderatable;

use Randomlaunch\Moderation\Flaggable\Flaggable;
use Randomlaunch\Moderation\Flaggable\FlaggableScope;
use Randomlaunch\Moderation\Approvable\Approvable;
use Randomlaunch\Moderation\Approvable\ApprovableScope;
use Randomlaunch\Moderation\Pendable\Pendable;
use Randomlaunch\Moderation\Pendable\PendableScope;

trait Moderatable
{
    use Flaggable, Approvable, Pendable;

    /**
     * Which scopes needs to be enabled on thie model
     *
     * @var string
     */
    protected $moderateDefaultScopes = [ApprovableScope::class];

    /**
     * Overwrite the sub-trait
     *
     * @return boolnea
     */
    public function applyFlaggableScope()
    {
        return in_array(FlaggableScope::class, $this->moderateDefaultScopes());
    }

    /**
     * Overwrite the sub-trait
     *
     * @return boolnea
     */
    public function applyApprovableScope()
    {
        return in_array(ApprovableScope::class, $this->moderateDefaultScopes());
    }

    /**
     * Overwrite the sub-trait
     *
     * @return boolnea
     */
    public function applyPendableScope()
    {
        return in_array(PendableScope::class, $this->moderateDefaultScopes());
    }

    /**
     * Overwrite the default scopes
     *
     * @return boolnea
     */
    public function moderateDefaultScopes()
    {
        return $this->moderateDefaultScopes;
    }

}
