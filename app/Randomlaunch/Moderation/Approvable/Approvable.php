<?php

namespace Randomlaunch\Moderation\Approvable;

use Randomlaunch\Moderation\Approvable\ApprovableScope;

trait Approvable
{
    /**
     * Which scope is associated with this trait
     *
     * @var string
     */
    protected static $approvableScopeClass = ApprovableScope::class;

    /**
     * Indicates if the model needs to apply the scope automatically.
     *
     * @var boolean
     */
    protected $applyApprovableScope = false;

    /**
     * Indicate which macro the attach on global scope.
     *
     * @var boolean
     */
    protected $defaultApprovableMacro = 'onlyApproved';

    /**
     * Indicate which macro the remove on global scope.
     *
     * @var boolean
     */
    protected $removeDefaultApprovableMacro = 'withApproved';

    /**
     * Boot the approvable trait for a model.
     *
     * @return void
     */
    public static function bootApprovable()
    {
        static::addGlobalScope(new static::$approvableScopeClass);
    }

    /**
     * Get the name of the column for applying the scope.
     *
     * @return string
     */
    public function getApprovedColumn()
    {
        return defined('static::APPROVED_COLUMN') ? static::APPROVED_COLUMN : 'approved_at';
    }

    /**
     * Get the fully qualified column name for applying the scope.
     *
     * @return string
     */
    public function getQualifiedApprovedColumn()
    {
        return $this->getTable().'.'.$this->getApprovedColumn();
    }

    /**
     * Determine if the model instance has been approved.
     *
     * @return boolean
     */
    public function isApproved()
    {
        return ! is_null($this->{$this->getApprovedColumn()});
    }

    /**
     * Approve this model instance.
     *
     * @return bool|null
     */
    public function approve()
    {
        $this->{$this->getApprovedColumn()} = $this->freshTimestamp();
        return $this->save();
    }

    /**
     * Unapprove this model instance.
     *
     * @return bool|null
     */
    public function unapprove()
    {
        $this->{$this->getApprovedColumn()} = null;
        return $this->save();
    }

    /**
     * Get a new query builder that excludes approved results.
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public static function withoutApproved()
    {
        return (new static)->newQueryWithoutScope(new static::$approvableScopeClass)
            ->withoutApproved();
    }

    /**
     * Get a new query builder that includes approved results.
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public static function withApproved()
    {
        return (new static)->newQueryWithoutScope(new static::$approvableScopeClass)
            ->withApproved();
    }

    /**
     * Get a new query builder that only includes approved results.
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public static function onlyApproved()
    {
        return (new static)->newQueryWithoutScope(new static::$approvableScopeClass)
            ->onlyApproved();
    }

    /**
     * Check if auto apply global scope is set on this model instance.
     *
     * @return boolean
     */
    public function applyApprovableScope()
    {
        return $this->applyApprovableScope;
    }

    /**
     * Get the default macro of this trait
     *
     * @return string
     */
    public function defaultApprovableMacro()
    {
        return $this->defaultApprovableMacro;
    }

    /**
     * Get the default remove macro of this trait
     *
     * @return string
     */
    public function removeDefaultApprovableMacro()
    {
        return $this->removeDefaultApprovableMacro;
    }

}
