<?php

namespace Randomlaunch\Moderation\Approvable;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ScopeInterface;

class ApprovableScope implements ScopeInterface
{
    /**
     * All of the extensions to be added to the builder.
     *
     * @var array
     */
    protected $extensions = ['WithApproved', 'WithoutApproved', 'OnlyApproved'];

    /**
     * Apply scope on the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder  $builder
     * @param \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        // Apply the extensions, so we can use it early
        $this->extend($builder);

        // in case of turning this scope off manually, don't need to apply
        if ($model->applyApprovableScope() && $model->defaultApprovableMacro()) {
            $builder->{$model->defaultApprovableMacro()}();
        }
    }

    /**
     * Remove scope from the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder  $builder
     * @param \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function remove(Builder $builder, Model $model)
    {
        if ($model->removeDefaultApprovableMacro()) {
            $builder->{$model->removeDefaultApprovableMacro()}();
        }
    }

    /**
     * Extend the query builder with the needed functions.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    public function extend(Builder $builder)
    {
        foreach ($this->extensions as $extension) {
            $this->{"add{$extension}"}($builder);
        }
    }

    /**
     * Add the with-approved extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addWithApproved(Builder $builder)
    {
        $builder->macro('withApproved', function (Builder $builder) {

            $column = $this->extractModelColumn($builder);

            $query = $builder->getQuery();

            $query->wheres = collect($query->wheres)
                ->reject(function ($where) use ($column) {
                    return $where['column'] == $column;
                }
            )->values()->all();

            return $builder;
        });
    }

    /**
     * Add the without-approved extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addWithoutApproved(Builder $builder)
    {
        $builder->macro('withoutApproved', function (Builder $builder) {

            $column = $this->extractModelColumn($builder);

            $builder->whereNull($column);

            return $builder;
        });
    }

    /**
     * Add the only-approved extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addOnlyApproved(Builder $builder)
    {
        $builder->macro('onlyApproved', function (Builder $builder) {

            $column = $this->extractModelColumn($builder);

            $builder->getQuery()->whereNotNull($column);

            return $builder;
        });
    }

    /**
     * Extract the qualified collumn name from builder
     *
     * @param  Builder $builder
     * @return string
     */
    protected function extractModelColumn(Builder $builder)
    {
        return $builder->getModel()->getQualifiedApprovedColumn();
    }

}
