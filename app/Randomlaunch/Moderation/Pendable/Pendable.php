<?php

namespace Randomlaunch\Moderation\Pendable;

use Randomlaunch\Moderation\Pendable\PendableScope;

trait Pendable
{
    /**
     * Which scope is associated with this trait
     *
     * @var string
     */
    protected static $pendableScopeClass = PendableScope::class;

    /**
     * Indicates if the model needs to apply the scope automatically.
     *
     * @var boolean
     */
    protected $applyPendableScope = false;

    /**
     * Indicate which macro the attach on global scope.
     *
     * @var boolean
     */
    protected $defaultPendableMacro = '';

    /**
     * Indicate which macro the remove on global scope.
     *
     * @var boolean
     */
    protected $removeDefaultPendableMacro = '';

    /**
     * Boot the pendable trait for a model.
     *
     * @return void
     */
    public static function bootPendable()
    {
        static::addGlobalScope(new static::$pendableScopeClass);
    }

    /**
     * Determine if the model instance is pending.
     *
     * @return boolean
     */
    public function isPending()
    {
        return ! $this->isApproved() && $this->isFlagged();
    }

    /**
     * Pending this model instance.
     *
     * @return bool|null
     */
    public function pending()
    {
        $this->{$this->getApprovedColumn()} = null;
        $this->{$this->getFlaggedColumn()} = null;
        return $this->save();
    }

    /**
     * Get a new query builder that only includes pending results.
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public static function onlyPending()
    {
        return (new static)->newQueryWithoutScope(new static::$pendableScopeClass)
            ->onlyPending();
    }

    /**
     * Check if auto apply global scope is set on this model instance.
     *
     * @return boolean
     */
    public function applyPendableScope()
    {
        return $this->applyPendableScope;
    }

    /**
     * Get the default macro of this trait
     *
     * @return string
     */
    public function defaultPendableMacro()
    {
        return $this->defaultPendableMacro;
    }

    /**
     * Get the default remove macro of this trait
     *
     * @return string
     */
    public function removeDefaultPendableMacro()
    {
        return $this->removeDefaultPendableMacro;
    }

}
