<?php

namespace Randomlaunch\Moderation\Pendable;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ScopeInterface;

class PendableScope implements ScopeInterface
{
    /**
     * All of the extensions to be added to the builder.
     *
     * @var array
     */
    protected $extensions = ['OnlyPending'];

    /**
     * Apply scope on the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder  $builder
     * @param \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        // Apply the extensions, so we can use it early
        $this->extend($builder);

        // in case of turning this scope off manually, don't need to apply
        if ($model->applyPendableScope() && $model->defaultPendableMacro()) {
            $builder->{$model->defaultPendableMacro()}();
        }
    }

    /**
     * Remove scope from the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder  $builder
     * @param \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function remove(Builder $builder, Model $model)
    {
        if ($model->removeDefaultPendableMacro()) {
            $builder->{$model->removeDefaultPendableMacro()}();
        }
    }

    /**
     * Extend the query builder with the needed functions.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    public function extend(Builder $builder)
    {
        foreach ($this->extensions as $extension) {
            $this->{"add{$extension}"}($builder);
        }
    }

    /**
     * Add the only-pending extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addOnlyPending(Builder $builder)
    {
        $builder->macro('onlyPending', function (Builder $builder) {

            $model = $builder->getModel();

            $builder->getQuery()
                ->whereNull($model->getQualifiedApprovedColumn())
                ->whereNull($model->getQualifiedFlaggedColumn());

            return $builder;
        });
    }

}
