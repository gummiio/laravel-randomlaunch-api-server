<?php

namespace Randomlaunch\Page\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL;

class PageNotFound extends Event
{
    use SerializesModels;

    /**
     * Info Object
     *
     * @var object
     */
    public $data;

    /**
     * Create a new event instance.
     *
     * @param Request $request Request Object
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->data = [
            'request_url' => URL::previous(),
            'ip'          => $request->ip(),
            'method'      => $request->method()
        ];
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
