<?php

namespace Randomlaunch\Page\Listeners\PageNotFound;

use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Randomlaunch\Helpers\SlackHelper as Slack;
use Randomlaunch\Page\Events\PageNotFound;

class NotifyOnSlack implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PageNotFound  $event
     * @return void
     */
    public function handle(PageNotFound $event)
    {
        $data = $event->data;

        Slack::new()
            ->text('*Page Not Found.*')
            ->color('danger')
            ->addField([
                "*Request*: `{$data['method']}` {$data['request_url']}",
                "*IP*: `{$data['ip']}`",
                "*At*: ".Carbon::now()->toDateTimeString()
            ])
            ->sendTo('#randomlaunch-activity');
    }
}
