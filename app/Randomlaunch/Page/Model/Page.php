<?php

namespace Randomlaunch\Page\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Randomlaunch\Model;

class Page extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_title', 'page_content', 'page_slug'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function setPageSlugAttribute($value)
    {
        $this->attributes['page_slug'] = str_slug($value);
    }
}
