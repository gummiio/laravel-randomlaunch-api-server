<?php

namespace Randomlaunch\Api\Transformer;

use Illuminate\Database\Eloquent\Model;

abstract class Transformer
{
    protected $transformed;

    protected $hashColumns = [];

    protected $hiddenColumns = [];

    protected $transformedKeys = [
        // key => new key
    ];

    public function __construct($transformed)
    {
        $this->transformed = $transformed;
    }

    public function transform()
    {
        $this->transformHiddenColumns()
            ->transformColumnKeys();

        return $this->transformed;
    }

    protected function transformHiddenColumns()
    {
        foreach ($this->hiddenColumns as $column) {
            if (array_key_exists($column, $this->transformed)) {
                unset($this->transformed[$column]);
            }
        }

        return $this;
    }

    protected function transformColumnKeys()
    {
        foreach ($this->transformedKeys as $column => $newKey) {
            if (array_key_exists($column, $this->transformed)) {
                $this->transformed[$newKey] = $this->transformed[$column];
                unset($this->transformed[$column]);
            }
        }

        return $this;
    }
}
