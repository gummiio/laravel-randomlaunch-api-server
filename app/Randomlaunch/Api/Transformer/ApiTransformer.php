<?php

namespace Randomlaunch\Api\Transformer;

use Randomlaunch\Api\Transformer\Transformer;
use Randomlaunch\Model;

class ApiTransformer extends Transformer
{
    protected $hashColumns = ['id'];
}
