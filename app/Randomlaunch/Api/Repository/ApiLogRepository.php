<?php

namespace Randomlaunch\Api\Repository;

use Randomlaunch\Api\Model\ApiLog;
use Randomlaunch\Repository;

class ApiLogRepository extends Repository
{

    public static function generateFromCurrentSession($request, $response)
    {
        $apiGuard = app('ApiGuard');
        $authType = $apiGuard->getApiKey()->for_admin? 'admin' : 'app';

        $header = collect($request->header())
            ->except(['cookie', 'php-auth-user', 'php-auth-pw']);

        $apiLog = ApiLog::create([
            'api_token'    => $apiGuard->getToken(),
            'endpoint'     => $request->path(),
            'method'       => $request->method(),
            'headers'      => json_encode($header),
            'params'       => json_encode($request->except('api_token')),
            'ip'           => $request->ip(),
            'user_agent'   => $request->server('HTTP_USER_AGENT'),
            'respond_code' => $response->getStatusCode(),
            'respond_time' => microtime(true) - LARAVEL_START
        ]);

        if (! Auth($authType)->guest()) {
            $apiLog->auth_user_id = Auth($authType)->id();
        }

        return $apiLog;
    }
}
