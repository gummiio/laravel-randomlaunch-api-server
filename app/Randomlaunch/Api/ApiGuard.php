<?php

namespace Randomlaunch\Api;

use App\Http\Requests\Request;
use Randomlaunch\Api\Model\ApiKey;


class ApiGuard
{
    /**
     * Api Token from the request header
     *
     * @var String
     */
    protected $requestToken = false;

    /**
     * Api Key Object
     *
     * @var Object
     */
    protected $apiKey = false;

    /**
     * If api key's limit has exceeded
     *
     * @var Object
     */
    protected $limitExceeded;

    /**
     * Try to fetch the token from header
     *
     * @return Boolean
     */
    public function fetchApiToken()
    {
        return !! $this->getToken();
    }

    /**
     * Check the current api key status
     *
     * @return Boolean
     */
    public function verifiedApiToken($admin = false)
    {
        return !! $this->getApiKey($admin);
    }

    /**
     * Check if current api have access quota
     *
     * @return boolean
     */
    public function apiTokenLimitExceeded()
    {
        if (null !== $this->limitExceeded) {
            return $this->limitExceeded;
        }

        return $this->limitExceeded = is_null($this->apiKey->limit)?
            false :
            $this->apiKey->countCurrentLimit() >= $this->apiKey->limit;
    }

    /**
     * Retreive the Api Key Object
     *
     * @return Object
     */
    public function getApiKey($admin = false)
    {
        if (false !== $this->apiKey) {
            return $this->apiKey;
        }

        return $this->apiKey = ApiKey::where('key', $this->getToken())
            ->where('for_admin', !!$admin)
            ->first();
    }

    /**
     * Get the header api key string
     *
     * @return string
     */
    public function getToken()
    {
        if (false !== $this->requestToken) {
            return $this->requestToken;
        }

        return $this->requestToken = Request()->header(Config()->get('api.header_key'));
    }

    /**
     * Check if current key has access to ...
     *
     * @param  string|array  $access
     * @return boolean
     */
    public function allow($access)
    {
        if (! $this->verified()) return false;
        if ($this->hasGlobalAccess()) return true;

        $accesses = [];

        if (is_string($access) || is_array($access)) {
            $accesses = is_array($access)? $access : [$access];
        }

        $found = $this->getApiKey()->scopes->filter(function ($item) use ($accesses) {
            return in_array($item->name, $accesses);
        })->count();

        return $accesses && $found === count($accesses);
    }

    /**
     * Check if current key has global access
     *
     * @return boolean
     */
    protected function hasGlobalAccess()
    {
        return !! $this->getApiKey()->scopes->first(function($key, $item) {
            return $item->name == '*';
        });
    }

    /**
     * return the value based on permission
     *
     * @return boolean
     */
    public function eitherOr($scope, $allow = true, $failed = false)
    {
        return $this->allow($scope)? $allow : $failed;
    }
}
