<?php

namespace Randomlaunch\Api\Middleware;

use Closure;

class ApiKeyValidate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $admin = false)
    {
        if (! app('ApiGuard')->fetchApiToken()) {
            return Response()->apiError('Whoops, looks like something went wrong.', [
                'type'    => 'ApiTokenNotFound',
                'code'    => 'AUTH-001',
                'message' => 'Unable to find api token.'
            ], 401);
        }

        if (! app('ApiGuard')->verifiedApiToken($admin)) {
            return Response()->apiError('Whoops, looks like something went wrong.', [
                'type'    => 'ApiTokenInvalid',
                'code'    => 'AUTH-002',
                'message' => 'Provided api token is invalid for the current request.'
            ], 401);
        }

        return $next($request);
    }
}
