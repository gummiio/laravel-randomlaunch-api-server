<?php

namespace Randomlaunch\Api\Middleware;

use Closure;
use Randomlaunch\Api\Events\ApiTokenLimitExceeded;

class ApiCheckLimit
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @throws ExceedAppTokenLimit
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (app('ApiGuard')->apiTokenLimitExceeded()) {
            event(new ApiTokenLimitExceeded(app('ApiGuard')->getApiKey()));

            return Response()->apiError('Whoops, looks like something went wrong.', [
                'type'    => 'ApiTokenLimitExceeded',
                'code'    => 'AUTH-003',
                'message' => 'Provided api token has reached its request limit.'
            ], 429);
        }

        return $next($request);
    }
}
