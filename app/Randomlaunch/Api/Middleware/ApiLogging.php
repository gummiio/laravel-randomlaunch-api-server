<?php

namespace Randomlaunch\Api\Middleware;

use Closure;
use Config;
use Randomlaunch\Api\Events\LongApiRespondTimeExecuted;
use Randomlaunch\Api\Model\ApiLog;
use Randomlaunch\Api\Repository\ApiLogRepository;

class ApiLogging
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * Log the request to the database
     *
     * @param  $request
     * @param  $response
     * @return void
     */
    public function terminate($request, $response)
    {
        $apiGuard = app('ApiGuard');

        if ($apiGuard->verifiedApiToken() && ! $apiGuard->apiTokenLimitExceeded()) {
            $apiLog = ApiLogRepository::generateFromCurrentSession($request, $response);
            $apiLog->save();
            $apiGuard->getApiKey()->logs()->save($apiLog);

            if ($apiLog->respond_time > Config::get('api.long_request_threshold')) {
                event(new LongApiRespondTimeExecuted($apiLog));
            }
        }
    }
}
