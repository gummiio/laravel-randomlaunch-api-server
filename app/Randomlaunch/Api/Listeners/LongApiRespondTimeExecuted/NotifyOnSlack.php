<?php

namespace Randomlaunch\Api\Listeners\LongApiRespondTimeExecuted;

use Config;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Randomlaunch\Api\Events\LongApiRespondTimeExecuted;
use Randomlaunch\Helpers\SlackHelper as Slack;

class NotifyOnSlack
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LongApiRespondTimeExecuted  $event
     * @return void
     */
    public function handle(LongApiRespondTimeExecuted $event)
    {
        $apiLog = $event->apiLog;

        Slack::new()
            ->text(sprintf('*An Api request is talking for more than %d secdons.*', Config::get('api.long_request_threshold')))
            ->color('warning')
            ->addField([
                "*Log ID*: {$apiLog->id}",
                "*Request:* `{$apiLog->method}` _{$apiLog->endpoint}_",
                "*Response:* `{$apiLog->respond_code}` _{$apiLog->respond_time}s_",
                "*At:* ".$apiLog->created_at->toDateTimeString()
            ])
            ->send();
    }
}
