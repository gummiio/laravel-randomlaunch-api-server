<?php

namespace Randomlaunch\Api\Listeners\ApiTokenLimitExceeded;

use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Randomlaunch\Api\Events\ApiTokenLimitExceeded;
use Randomlaunch\Helpers\SlackHelper as Slack;

class NotifyOnSlack implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApiTokenLimitExceeded  $event
     * @return void
     */
    public function handle(ApiTokenLimitExceeded $event)
    {
        $apiKey = $event->apiKey;

        Slack::new()
            ->text('*An Api Token has exceed its limit.*')
            ->color('warning')
            ->addField([
                "*Api ID*: {$apiKey->id}",
                "*Key*: `{$apiKey->key}`",
                "*At*: ".Carbon::now()->toDateTimeString()
            ])
            ->send();
    }
}
