<?php

namespace Randomlaunch\Api\Model;

use Randomlaunch\Api\Model\ApiScope;
use Randomlaunch\Model;

class ApiScopeGroup extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'api_scope_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'label', 'description'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the keywords
     *
     * @return Object
     */
    public function scopes() {
        return $this->hasMany(ApiScope::class);
    }
}
