<?php

namespace Randomlaunch\Api\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Randomlaunch\Api\Model\ApiLog;
use Randomlaunch\Api\Model\ApiScope;
use Randomlaunch\Model;

class ApiKey extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'api_keys';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'limit', 'for_admin', 'allow_guest'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['scopes'];

    /**
     * Get this key's Logs
     *
     * @return Object
     */
    public function logs()
    {
        return $this->hasMany(ApiLog::class);
    }

    /**
     * Get this scopes
     *
     * @return Object
     */
    public function scopes()
    {
        return $this->belongsToMany(ApiScope::class);
    }

    /**
     * Count the current timeframe's limit
     * @return int
     */
    public function countCurrentLimit()
    {
        $hour_ago = new Carbon('-1 hour');
        return $this->logs()->where('created_at', '>=', $hour_ago)->count();
    }
}
