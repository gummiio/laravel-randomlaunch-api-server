<?php

namespace Randomlaunch\Api\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Randomlaunch\Api\Model\ApiKey;
use Randomlaunch\Api\Model\ApiScopeGroup;
use Randomlaunch\Model;

class ApiScope extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'api_scopes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['api_scope_group_id', 'name', 'label', 'description'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get this keys
     *
     * @return Object
     */
    public function keys() {
        return $this->belongsToMany(ApiKey::class);
    }

    /**
     * Get this scope type
     *
     * @return Object
     */
    public function scopeGroup() {
        return $this->belongsTo(ApiScopeGroup::class, 'api_scope_group_id');
    }
}
