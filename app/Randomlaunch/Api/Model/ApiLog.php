<?php

namespace Randomlaunch\Api\Model;

use Randomlaunch\Access\Model\AdminUser;
use Randomlaunch\Access\Model\User;
use Randomlaunch\Api\Model\ApiKey;
use Randomlaunch\Model;

class ApiLog extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'api_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'api_token', 'auth_user_id', 'endpoint', 'method',
        'headers', 'params', 'ip', 'user_agent',
        'respond_code', 'respond_time'
    ];


    /**
     * Get api key
     *
     * @return Object
     */
    public function apiKey()
    {
        return $this->belongsTo(ApiKey::class)->withTrashed();
    }

    /**
     * Get auth user
     *
     * @return Object
     */
    public function getAuthUser()
    {
        if ( ! $this->apiKey) return null;

        if ($this->apiKey->for_admin) {
            return $this->belongsTo(AdminUser::class)->withTrashed();
        } else {
            return $this->belongsTo(User::class)->withTrashed();
        }
    }
}
