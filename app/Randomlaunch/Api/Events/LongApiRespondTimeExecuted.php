<?php

namespace Randomlaunch\Api\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Randomlaunch\Api\Model\ApiLog;

class LongApiRespondTimeExecuted extends Event
{
    use SerializesModels;

    /**
     * Api Log Object
     *
     * @var object
     */
    public $apiLog;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ApiLog $apiLog)
    {
        $this->apiLog = $apiLog;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
