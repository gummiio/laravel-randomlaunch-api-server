<?php

namespace Randomlaunch\Api\Events;

use App\Events\Event;
use Randomlaunch\Api\Model\ApiKey;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class ApiTokenLimitExceeded extends Event
{
    use SerializesModels;

    /**
     * Api Key Object
     *
     * @var object
     */
    public $apiKey;

    /**
     * Create a new event instance.
     *
     * @param ApiKey $apiKey ApiKey Object
     * @return void
     */
    public function __construct(ApiKey $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
