<?php

namespace Randomlaunch\Link\Model;

use Randomlaunch\Model;

class BlackList extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'blacklists';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'pattern'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the specific type
     *
     * @return Object
     */
    public function scopeOfType($query, $type = 'regular')
    {
        return $query->where('type', $type);
    }

}
