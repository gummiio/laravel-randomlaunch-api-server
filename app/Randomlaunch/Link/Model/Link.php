<?php

namespace Randomlaunch\Link\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Randomlaunch\Likable\Traits\Likable;
use Randomlaunch\Link\Events\LaunchedLinkNotFound;
use Randomlaunch\Link\Model\Category;
use Randomlaunch\Link\Model\Keyword;
use Randomlaunch\Media\Model\Media;
use Randomlaunch\Media\Traits\HasMedias;
use Randomlaunch\Model;
use Randomlaunch\Moderation\Moderatable\Moderatable;
use Randomlaunch\Reportable\Reportable;


class Link extends Model
{
    use SoftDeletes, Moderatable, Likable, Reportable, HasMedias;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'links';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'domain', 'effective_url', 'source_url',
        'title', 'description', 'load_time',
        'desktop_screenshot', 'mobile_screenshot',
        'click_stats'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['launched_at', 'approved_at', 'flagged_at', 'deleted_at'];

    protected $with = ['medias'];

    protected $hidden = ['medias'];

    /**
     * Overwrite the default scopes
     *
     * @return boolnea
     */
    public function moderateDefaultScopes()
    {
        return [];
    }

    /**
     * Get the keyword
     *
     * @return Object
     */
    public function keyword()
    {
        return $this->belongsTo(Keyword::class);
    }

    /**
     * Get the category
     *
     * @return Object
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getDesktopScreenshotAttribute($value)
    {
        return $this->getMediaById($value);
    }

    public function getMobileScreenshotAttribute($value)
    {
        return $this->getMediaById($value);
    }

    public function launch(Carbon $launchTime)
    {
        $this->launched_at = $launchTime;

        return $this->save();
    }

    public static function getCurrentLaunch()
    {
        $link = static::Orderby('launched_at', 'desc')
            ->where('launched_at', '<', Carbon::now())
            ->first();

        if (! $link) {
            event(new LaunchedLinkNotFound(Carbon::now()));
        }

        return $link;
    }
}
