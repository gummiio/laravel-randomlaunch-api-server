<?php

namespace Randomlaunch\Link\Model;

use Randomlaunch\Link\Model\Keyword;
use Randomlaunch\Link\Model\Link;
use Randomlaunch\Model;

class Category extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'label', 'description'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = str_slug($value);
    }

    /**
     * Get the keywords
     *
     * @return Object
     */
    public function keywords() {
        return $this->hasMany(Keyword::class);
    }

    /**
     * Get the links
     *
     * @return Object
     */
    public function links() {
        return $this->hasManyThrough(Link::class, Keyword::class);
    }

}
