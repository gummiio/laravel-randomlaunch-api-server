<?php

namespace Randomlaunch\Link\Providers;

use Illuminate\Support\ServiceProvider;
use Randomlaunch\Link\Events\KeywordCreated;
use Randomlaunch\Link\Events\KeywordDeleted;
use Randomlaunch\Link\Events\LinkCreated;
use Randomlaunch\Link\Events\LinkDeleted;
use Randomlaunch\Link\Model\Keyword;
use Randomlaunch\Link\Model\Link;

class LinkServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        Link::created(function ($link) {
            event(new LinkCreated($link));
        });

        Link::deleted(function ($link) {
            event(new LinkDeleted($link->id));
        });

        Keyword::created(function ($keyword) {
            event(new KeywordCreated($keyword));
        });

        Keyword::deleted(function ($keyword) {
            event(new KeywordDeleted($keyword->id));
        });
    }

    public function register()
    {

    }
}
