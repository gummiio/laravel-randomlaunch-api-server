<?php

namespace Randomlaunch\Link\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Randomlaunch\Link\Model\Link;

class LinkScreenshotGenerationCompleted extends Event
{
    use SerializesModels;

    /**
     * Keyword Object
     *
     * @var object
     */
    public $link;
    public $desktopLocation;
    public $mobileLocation;

    /**
     * Create a new event instance.
     *
     * @param Keyword $link Keyword Object
     * @return void
     */
    public function __construct(Link $link, $desktopLocation = '', $mobileLocation = '')
    {
        $this->link = $link;
        $this->desktopLocation = $desktopLocation;
        $this->mobileLocation = $mobileLocation;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
