<?php

namespace Randomlaunch\Link\Events;

use App\Events\Event;
use Carbon\Carbon;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class LaunchedLinkNotFound extends Event
{
    use SerializesModels;

    /**
     * Carbon Object
     *
     * @var object
     */
    public $time;

    /**
     * Create a new event instance.
     *
     * @param Carbon $time Carbon Object
     * @return void
     */
    public function __construct(Carbon $time)
    {
        $this->time = $time;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
