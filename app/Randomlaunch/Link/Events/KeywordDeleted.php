<?php

namespace Randomlaunch\Link\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class KeywordDeleted extends Event
{
    use SerializesModels;

    /**
     * Link Object
     *
     * @var object
     */
    public $keywordId;

    /**
     * Create a new event instance.
     *
     * @param Keyword $keywordId Link Object
     * @return void
     */
    public function __construct($keywordId)
    {
        $this->keywordId = $keywordId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
