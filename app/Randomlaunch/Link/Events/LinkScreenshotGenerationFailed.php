<?php

namespace Randomlaunch\Link\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Randomlaunch\Link\Model\Keyword;
use Randomlaunch\Link\Model\Link;

class LinkScreenshotGenerationFailed extends Event
{
    use SerializesModels;

    /**
     * Keyword Object
     *
     * @var object
     */
    public $link;

    /**
     * Create a new event instance.
     *
     * @param Keyword $link Keyword Object
     * @return void
     */
    public function __construct(Link $link)
    {
        $this->link = $link;
        Log::info('generation failed');
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
