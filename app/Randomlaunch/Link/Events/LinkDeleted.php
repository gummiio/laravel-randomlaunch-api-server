<?php

namespace Randomlaunch\Link\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Randomlaunch\Link\Model\Link;

class LinkDeleted extends Event
{
    use SerializesModels;

    /**
     * Link Object
     *
     * @var object
     */
    public $linkId;

    /**
     * Create a new event instance.
     *
     * @param Link $linkId Link Object
     * @return void
     */
    public function __construct($linkId)
    {
        $this->linkId = $linkId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
