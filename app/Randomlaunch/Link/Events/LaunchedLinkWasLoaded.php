<?php

namespace Randomlaunch\Link\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Randomlaunch\Link\Model\Link;

class LaunchedLinkWasLoaded extends Event
{
    use SerializesModels;

    /**
     * Link Object
     *
     * @var object
     */
    public $link;

    /**
     * Create a new event instance.
     *
     * @param Link $link Link Object
     * @return void
     */
    public function __construct(Link $link)
    {
        $this->link = $link;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
