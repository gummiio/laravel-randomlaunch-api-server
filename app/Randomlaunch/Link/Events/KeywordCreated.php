<?php

namespace Randomlaunch\Link\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Randomlaunch\Link\Model\Keyword;

class KeywordCreated extends Event
{
    use SerializesModels;

    /**
     * Keyword Object
     *
     * @var object
     */
    public $keyword;

    /**
     * Create a new event instance.
     *
     * @param Keyword $keyword Keyword Object
     * @return void
     */
    public function __construct(Keyword $keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
