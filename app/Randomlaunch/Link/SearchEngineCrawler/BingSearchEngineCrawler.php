<?php

namespace Randomlaunch\Link\SearchEngineCrawler;

use Randomlaunch\Link\SearchEngineCrawler\SearchEngineCrawler;

class BingSearchEngineCrawler extends SearchEngineCrawler
{

    protected function buildSearchQueryUrl($search_keyword)
    {
        $url = 'https://www.bing.com/search?';
        $query_args = [
            'q'     => urlencode($search_keyword),  // search query
            'count' => 30,                          // number of page
            'first' => rand(1, 5) * 10 +1           // page 5
        ];

        $query_str = http_build_query($query_args);

        return $url . $query_str;
    }


    protected function extractSearchResults($crawler)
    {
        $results = [];
        $links = $crawler->filter('#b_results h2 a');

        foreach( $links as $link ) {
            $result_url = $link->getAttribute('href');
            if ($result_url) $this->addResult($result_url);
        }
    }

}
