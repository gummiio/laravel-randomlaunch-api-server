<?php

namespace Randomlaunch\Link\SearchEngineCrawler;

use Symfony\Component\DomCrawler\Crawler;

abstract class SearchEngineCrawler
{

    protected $crawler;
    protected $search_keyword;
    protected $search_url;
    protected $results = [];

    public function __construct($search_keyword = '')
    {
        $this->setSearchKeyword($search_keyword);

        $this->search_url = $this->buildSearchQueryUrl($this->search_keyword);

        $this->crawler = new Crawler;
        $this->crawler->addContent(file_get_contents($this->search_url));

        $this->extractSearchResults($this->crawler);
    }

    public function setSearchKeyword($search_keyword='')
    {
        $this->search_keyword = trim($search_keyword);
        return $this;
    }

    protected function addResult($url)
    {
        $this->results[] = $url;
        return $this;
    }

    public function getResults()
    {
        return $this->results;
    }

    abstract protected function buildSearchQueryUrl($search_keyword);

    abstract protected function extractSearchResults($crawler);
}
