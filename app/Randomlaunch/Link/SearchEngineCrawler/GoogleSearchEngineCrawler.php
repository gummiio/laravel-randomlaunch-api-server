<?php

namespace Randomlaunch\Link\SearchEngineCrawler;

use Randomlaunch\Link\SearchEngineCrawler\SearchEngineCrawler;

class GoogleSearchEngineCrawler extends SearchEngineCrawler
{

    protected function buildSearchQueryUrl($search_keyword)
    {
        $url = 'http://www.google.com/search?';
        $query_args = [
            'q'     => urlencode($search_keyword),  // search query
            'num'   => 30,                          // number of page
            'pws'   => 0,                           // personalized search
            'lr'    => 'lang_en',                   // language
            'start' => rand(1, 5) * 10              // page 5
        ];

        $query_str = http_build_query($query_args);

        return $url . $query_str;
    }


    protected function extractSearchResults($crawler)
    {
        $results = [];
        $links = $crawler->filter('h3.r a');

        foreach( $links as $link ) {
            $result_url = $this->parseRealUrl($link->getAttribute('href'));
            if ($result_url) $this->addResult($result_url);
        }
    }

    protected function parseRealUrl($url)
    {
        // not a web link
        if(substr($url, 0, 4) != '/url') return false;

        // add a random host so we can parse the query
        $parsed = parse_url('http://google.com' . $url);
        $parsed_query = $parsed['query']? $parsed['query'] : '';

        // get the query $q, which is the url link
        parse_str($parsed_query, $parsed);

        return isset($parsed['q'])? $parsed['q'] : false;
    }

}
