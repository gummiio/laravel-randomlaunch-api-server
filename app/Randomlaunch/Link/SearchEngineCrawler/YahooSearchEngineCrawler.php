<?php

namespace Randomlaunch\Link\SearchEngineCrawler;

use Randomlaunch\Link\SearchEngineCrawler\SearchEngineCrawler;

class YahooSearchEngineCrawler extends SearchEngineCrawler
{

    protected function buildSearchQueryUrl($search_keyword)
    {
        $url = 'https://ca.search.yahoo.com/search?';
        $query_args = [
            'p'     => urlencode($search_keyword),  // search query
            'pz'    => 30,                          // number of page
            'b'     => rand(1, 5) * 10 + 1          // page 5
        ];

        $query_str = http_build_query($query_args);

        return $url . $query_str;
    }


    protected function extractSearchResults($crawler)
    {
        $results = [];
        $links = $crawler->filter('.searchCenterMiddle .dd h3.title a');

        foreach( $links as $link ) {
            $result_url = $link->getAttribute('href');
            if ($result_url) $this->addResult($result_url);
        }
    }

}
