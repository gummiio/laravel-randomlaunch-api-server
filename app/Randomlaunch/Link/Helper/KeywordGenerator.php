<?php

namespace Randomlaunch\Link\Helper;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Ixudra\Curl\Facades\Curl;
use Randomlaunch\Link\Jobs\CreateNewKeyword;
use Randomlaunch\Link\Model\Keyword;

class KeywordGenerator
{
    use DispatchesJobs;

    protected $keyword;

    public function generate()
    {
        // http://randomword.setgetgo.com/
        $this->keyword = Curl::to('http://randomword.setgetgo.com/get.php')->get();

        return $this;
    }

    public function make()
    {
        $this->dispatch(new CreateNewKeyword($this->keyword));
    }

}
