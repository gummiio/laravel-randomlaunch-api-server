<?php

namespace Randomlaunch\Link\Helper;

use Carbon\Carbon;
use Randomlaunch\Link\Model\Category;
use Randomlaunch\Link\Model\Link;

class LinkLauncher
{
    public static $launchInterval = 4;

    public $category;

    public $link;

    public function category($category = '')
    {
        $this->category = $this->getCategory($category);

        return $this;
    }

    public function choose()
    {
        if (!$this->category) {
            $this->category();
        }

        // $this->link = Link::onlyApproved()
        $this->link = Link::whereNotNull('approved_at')
            ->whereNull('launched_at')
            ->where('category_id', $this->category->id)
            ->orderByRandom()
            ->first();

        // temporarily fallback if no approved is found
        if (! $this->link) {
            $this->link = Link::whereNull('launched_at')
                ->where('category_id', $this->category->id)
                ->where('load_time', '<', 1)
                ->whereNotNull('description')
                ->orderByRandom()
                ->first();
        }

        return $this;
    }

    public function launch()
    {
        if (! $this->needToLaunch()) return false;

        if (! $this->link) return false;

        return $this->link->launch($this->nextLaunchTime());
    }

    public function needToLaunch()
    {
        return Carbon::now()->hour % static::$launchInterval === static::$launchInterval - 1;
    }

    public function nextLaunchTime()
    {
        $now = Carbon::now();

        $remainder = $now->hour % static::$launchInterval;

        if ($remainder == 0) {
            $now->addHours(static::$launchInterval);
        } elseif ($remainder > 0) {
            $now->addHours(static::$launchInterval - $remainder);
        }

        return $now->minute(0)->second(0);
    }

    protected function getCategory($category)
    {
        if (! $category) {
            return Category::orderByRandom()->first();
        }

        return Category::where('name', $category)
            ->orWhere('label', $category)
            ->first();
    }
}
