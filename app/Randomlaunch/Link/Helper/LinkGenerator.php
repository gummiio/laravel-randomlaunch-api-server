<?php

namespace Randomlaunch\Link\Helper;

use Randomlaunch\Link\Jobs\CreateNewLink;
use Randomlaunch\Link\Model\Keyword;
use Randomlaunch\Link\SearchEngineCrawler\BingSearchEngineCrawler;
use Randomlaunch\Link\SearchEngineCrawler\GoogleSearchEngineCrawler;
use Randomlaunch\Link\SearchEngineCrawler\YahooSearchEngineCrawler;

class LinkGenerator
{
    public $crawlers = [
        GoogleSearchEngineCrawler::class,
        YahooSearchEngineCrawler::class,
        BingSearchEngineCrawler::class
    ];

    public $keyword;

    public $links = [];

    public function setKeyword(Keyword $keyword)
    {
        $this->keyword = $keyword;
        return $this;
    }

    protected function getRandomeKeyword()
    {
        $this->keyword = Keyword::with('category')->orderByRaw("RAND()")->first();
    }

    public function generate()
    {
        if (! $this->keyword) {
            $this->getRandomeKeyword();
        }

        foreach ($this->crawlers as $crawler) {
            $this->links = array_merge(
                $this->links,
                (new $crawler($this->keyword->label))->getResults()
            );
        }

        return $this;
    }

    public function makeLinks()
    {
        foreach ($this->links as $link) {
            dispatch(new CreateNewLink($link, $this->keyword));
        }

        return $this;
    }

}
