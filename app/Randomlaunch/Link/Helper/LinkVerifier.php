<?php

namespace Randomlaunch\Link\Helper;

use Randomlaunch\Link\Model\Link;
use Randomlaunch\Link\Model\Blacklist;
use Symfony\Component\DomCrawler\Crawler;

class LinkVerifier
{
    public $input_url;

    public $base_domain;

    public $effective_url;

    public $redirect_count;

    public $total_time;

    public $site_title;

    public $site_description;

    protected $site_body;

    protected static $black_lists_regular;

    protected static $black_lists_regexp;


    public function __construct($url)
    {
        $this->input_url = $url;
    }

    public function verify()
    {
        // basic check
        if (! $this->validUrl()) return false;

        // basic parse
        if (! $this->parseUrl()) return false;

        // check exists
        if ($this->linkExists()) return false;

        // check black lists
        if ($this->blackListed()) return false;

        // check respond
        if (! $this->checkRespond()) return false;

        // check it's html
        if (! $this->isValidHtml()) return false;

        // check it's english
        if (! $this->isEnglish()) return false;

        // check it's english
        if ($this->isLoremIpsum()) return false;

        // Grab the title / description
        $this->grabSiteInfo();

        return true;
    }

    protected function validUrl()
    {
        $url = $this->input_url;

        // first do some quick sanity checks:
        if (! $url || ! is_string($url)) return false;

        // quick check url is roughly a valid http request: ( http://blah/... )
        if (! preg_match('/^http(s)?:\/\/[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(\/.*)?$/i', $url)) return false;

        return true;
    }

    protected function parseUrl()
    {
        $url = $this->input_url;

        if (! $parsed = parse_url($url) ) return false;

        // replace ONLY www, we want the base if it's www
        $this->base_domain = strtolower(str_replace('www.', '', $parsed['host']));

        return true;
    }

    protected function linkExists()
    {
        return !! (new Link)->newQueryWithoutScopes()
            ->where('domain', 'like', "%{$this->base_domain}%")
            ->first();
    }

    protected function blackListed()
    {
        // only load once
        if (! static::$black_lists_regular || !static::$black_lists_regexp) {
            $this->loadBlackLists();
        }

        $url_peices = explode('.', $this->base_domain);

        // get black list as-is pattern
        $regular_found = static::$black_lists_regular->first(function($key, $value) use ($url_peices) {
            return in_array($value->pattern, $url_peices);
        });

        if ($regular_found) return true;

        // get black list regexp pattern
        $regexp_found = Static::$black_lists_regexp->first(function($kye, $value) {
            try {
                if (preg_match($value->pattern, $this->base_domain)) return true;
            } catch (\Exception $e) {
                return false;
            }
        });

        if ($regexp_found) return true;

        return false;
    }

    protected function checkRespond()
    {
        $url = $this->base_domain;

        if (! $ch = curl_init($url)) return false;

        curl_setopt($ch, CURLOPT_BUFFERSIZE     , 128);      // more progress info
        // curl_setopt($ch, CURLOPT_HEADER         , true);     // we want headers
        // curl_setopt($ch, CURLOPT_NOBODY         , true);     // dont need body
        curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);     // catch output (do NOT print!)
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION , true);
        curl_setopt($ch, CURLOPT_MAXREDIRS      , 10);       // fairly random number, but could prevent unwanted endless redirects with followlocation=true
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 10);       // fairly random number (seconds)... but could prevent waiting forever to get a result
        curl_setopt($ch, CURLOPT_TIMEOUT        , 10);       // fairly random number (seconds)... but could prevent waiting forever to get a result
        curl_setopt($ch, CURLOPT_USERAGENT      , "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1");   // pretend we're a regular browser

        $this->site_body = curl_exec($ch);

        if (! curl_errno($ch) && in_array(curl_getinfo($ch, CURLINFO_HTTP_CODE), [200,301,302])) {
            $this->effective_url    = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
            $this->redirect_count   = curl_getinfo($ch, CURLINFO_REDIRECT_COUNT);
            $this->total_time       = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
            curl_close($ch);
            return true;
        }

        curl_close($ch);
        return false;
    }

    protected function isValidHtml()
    {
        $crawler = new Crawler;
        $crawler->addContent($this->site_body);

        if (! $crawler->filter('html')->count()) return false;

        if (! $crawler->filter('head')->count()) return false;

        if (! $crawler->filter('body')->count()) return false;

        return true;
    }

    protected function isEnglish()
    {
        $crawler = new Crawler;
        $crawler->addContent($this->site_body);

        // if there's an lang set, it has to be english
        foreach ($crawler->filter('html[lang]') as $html ) {
            if( strpos($html->getAttribute('lang'), 'en') === false ) return false;
            break;
        }

        return true;
    }

    protected function isLoremIpsum()
    {
        $pureText = strip_tags($this->site_body);
        return str_contains('Lorem ipsum dolor sit amet', $pureText);
    }

    protected function grabSiteInfo()
    {
        $crawler = new Crawler;
        $crawler->addContent($this->site_body);

        $titles = $crawler->filter('title');
        foreach ($titles as $title) {
            if ($value = $title->nodeValue) {
                $this->site_title = trim($value);
                break;
            }
        }

        $descriptions = $crawler->filter('meta[name=description]');
        foreach ($descriptions as $description) {
            if ($value = $description->getAttribute('content')) {
                $this->site_description = trim($value);
                break;
            }
        }

        if (! $this->site_title) {
            $this->site_title = '(no title)';
        }
    }

    protected function loadBlackLists()
    {
        $blacklists = Blacklist::all();

        static::$black_lists_regular = $blacklists->filter(function($item){
            return $item->type == 'regular';
        });

        static::$black_lists_regexp = $blacklists->filter(function($item){
            return $item->type == 'regexp';
        });
    }

    public function retrieve()
    {
        return [
            'input_url' => $this->input_url,
            'base_domain' => $this->base_domain,
            'effective_url' => $this->effective_url,
            'redirect_count' => $this->redirect_count,
            'total_time' => $this->total_time,
            'site_title' => $this->site_title,
            'site_description' => $this->site_description,
        ];
    }
}
