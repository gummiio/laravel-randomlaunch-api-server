<?php

namespace Randomlaunch\Link\Listeners\LinkScreenshotGenerationCompleted;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Randomlaunch\Helpers\CloudinaryHelper;
use Randomlaunch\Link\Events\LinkScreenshotGenerationCompleted;
use Randomlaunch\Media\Model\Media;

class createLinkScreenshotMedias implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LinkScreenshotGenerationCompleted  $event
     * @return void
     */
    public function handle(LinkScreenshotGenerationCompleted $event)
    {
        $cloud_id = CloudinaryHelper::randomCloud();

        if ($event->desktopLocation) {
            $this->createLinkMedia($event->desktopLocation, $event->link, 'desktop_screenshot', $cloud_id);
        }

        if ($event->mobileLocation) {
            $this->createLinkMedia($event->mobileLocation, $event->link, 'mobile_screenshot', $cloud_id);
        }
    }

    protected function createLinkMedia($fileLocation, $link, $column, $cloud_id)
    {
        if (file_exists($fileLocation)) {

            $pathinfo = pathinfo($fileLocation);

            $media = Media::create([
                'filename'  => $pathinfo['filename'],
                'extension' => $pathinfo['extension'],
                'mime_type' => mime_content_type($fileLocation),
                'dir_path'  => $pathinfo['dirname'],
                'cloud_id'  => $cloud_id,
                'file_size' => filesize($fileLocation)
            ]);

            $link->$column = $media->id;
            $link->save();
        }
    }
}
