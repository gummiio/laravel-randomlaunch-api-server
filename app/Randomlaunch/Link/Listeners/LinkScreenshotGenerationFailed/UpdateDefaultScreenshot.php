<?php

namespace Randomlaunch\Link\Listeners\LinkScreenshotGenerationFailed;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Randomlaunch\Link\Events\LinkScreenshotGenerationFailed;

class UpdateDefaultScreenshot implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LinkScreenshotGenerationFailed  $event
     * @return void
     */
    public function handle(LinkScreenshotGenerationFailed $event)
    {

    }
}
