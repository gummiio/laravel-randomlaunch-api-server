<?php

namespace Randomlaunch\Link\Listeners\LaunchedLinkNotFound;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Randomlaunch\Link\Events\LaunchedLinkNotFound;

class RegenerateLaunch implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LaunchedLinkNotFound  $event
     * @return void
     */
    public function handle(LaunchedLinkNotFound $event)
    {
        $launcher = new \Randomlaunch\Link\Helper\LinkLauncher;
        if (! $launcher->needToLaunch()) return;

        $launcher->choose()->launch();
    }
}
