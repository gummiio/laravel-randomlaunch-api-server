<?php

namespace Randomlaunch\Link\Listeners\LaunchedLinkWasLoaded;

use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Randomlaunch\Link\Events\LaunchedLinkWasLoaded;

class RecordLaunchStatistic implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LaunchedLinkWasLoaded  $event
     * @return void
     */
    public function handle(LaunchedLinkWasLoaded $event)
    {
        $link = $event->link;
        $stats = $link->click_stats? : '[]';
        $stats = json_decode($stats);

        $stats[] = [
            'identifier' => md5(request()->ip() . request()->server('HTTP_USER_AGENT')),
            'time' => Carbon::now()->timestamp
        ];

        $link->click_stats = json_encode($stats);
        $link->save();
    }
}
