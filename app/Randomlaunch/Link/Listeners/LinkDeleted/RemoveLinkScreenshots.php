<?php

namespace Randomlaunch\Link\Listeners\LinkDeleted;

use App\Events\Randomlaunch\Link\Events\LinkDeleted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RemoveLinkScreenshots implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LinkDeleted  $event
     * @return void
     */
    public function handle(LinkDeleted $event)
    {
        //
    }
}
