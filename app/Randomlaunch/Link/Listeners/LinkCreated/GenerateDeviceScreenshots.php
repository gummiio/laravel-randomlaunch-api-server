<?php

namespace Randomlaunch\Link\Listeners\LinkCreated;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Randomlaunch\Helpers\BrowsershotHelper;
use Randomlaunch\Link\Events\LinkCreated;
use Randomlaunch\Link\Events\LinkScreenshotGenerationCompleted;
use Randomlaunch\Link\Events\LinkScreenshotGenerationFailed;
use Spatie\Browsershot\Browsershot;

class GenerateDeviceScreenshots implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LinkCreated  $event
     * @return void
     */
    public function handle(LinkCreated $event)
    {
        $link = $event->link;
        $browsershot = BrowsershotHelper::init($link->effective_url)->timeout(15000); // 15s

        try {
            event(new LinkScreenshotGenerationCompleted(
                $link,
                $browsershot->takeDesktop(),
                $browsershot->takeMobile("{$link->domain}-mobile")
            ));
        } catch (\Exception $e) {
            event(new LinkScreenshotGenerationFailed($link, $e));
            // $this->delete();
        }
    }
}
