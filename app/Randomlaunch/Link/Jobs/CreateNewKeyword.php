<?php

namespace Randomlaunch\Link\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Randomlaunch\Link\Model\Category;
use Randomlaunch\Link\Model\Keyword;

class CreateNewKeyword extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $keyword;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (! Keyword::where('label', $this->keyword)->first()) {
            $link = new Keyword([
                'name' => $this->keyword,
                'label' => str_slug($this->keyword),
                'description' => 'Generated from http://randomword.setgetgo.com.'
            ]);

            $keyword = Category::where('label', 'Random')
                ->first()
                ->keywords()
                ->save($link);
        }
    }
}
