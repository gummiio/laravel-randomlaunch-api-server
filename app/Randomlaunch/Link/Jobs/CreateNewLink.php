<?php

namespace Randomlaunch\Link\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Randomlaunch\Link\Helper\LinkVerifier;
use Randomlaunch\Link\Model\Keyword;
use Randomlaunch\Link\Model\Link;

class CreateNewLink extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $link;
    protected $keyword;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($link, Keyword $keyword)
    {
        $this->link = $link;
        $this->keyword = $keyword;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $verifier = new LinkVerifier($this->link);

        if ($verifier->verify()) {
            $link = new Link([
                'domain' => $verifier->base_domain,
                'effective_url' => strtolower($verifier->effective_url),
                'source_url' => $verifier->input_url,
                'title' => $verifier->site_title,
                'description' => $verifier->site_description,
                'load_time' => $verifier->total_time
            ]);

            $link->keyword_id = $this->keyword->id;
            $link->category_id = $this->keyword->category_id;
            $link->save();
        }
    }
}
