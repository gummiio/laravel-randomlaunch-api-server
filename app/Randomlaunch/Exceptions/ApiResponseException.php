<?php

namespace Randomlaunch\Exceptions;

use Exception;

class ApiResponseException extends Exception
{
    protected $errorObject;
    protected $extras = [];

    public function __construct($message, $errorObject, $statusCode)
    {
        parent::__construct($message, $statusCode);

        $this->setErrorObject($errorObject);
    }

    public function setErrorObject($errorObject)
    {
        $this->errorObject = $errorObject;

        return $this;
    }

    public function getErrorObject()
    {
        return $this->errorObject;
    }

    public function setExtras($value)
    {
        $this->extras = $value;

        return $this;
    }

    public function addExtra($key, $value)
    {
        $this->extras[$key] = $value;

        return $this;
    }

    public function getExtras()
    {
        return $this->extras;
    }
}
