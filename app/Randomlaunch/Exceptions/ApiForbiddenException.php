<?php

namespace Randomlaunch\Exceptions;

use Randomlaunch\Exceptions\ApiResponseException;

class ApiForbiddenException extends ApiResponseException
{
    public function __construct()
    {
        parent::__construct('Forbidden.', [
            'type'    => 'NotOwnByYou',
            'code'    => 'FORB-001',
            'message' => 'Unable to process the action because the resource doesn\'t belong to you.'
        ], 403);
    }
}
