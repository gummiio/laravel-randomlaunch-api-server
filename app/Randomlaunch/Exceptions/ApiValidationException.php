<?php

namespace Randomlaunch\Exceptions;

use Randomlaunch\Exceptions\ApiResponseException;

class ApiValidationException extends ApiResponseException
{
    public function __construct($validator)
    {
        parent::__construct('Invalid data.', [
            'type'    => 'ValidationFailed',
            'code'    => 'VALI-001',
            'message' => 'Unable to process the post data.'
        ], 422);

        $this->addExtra('validation', $validator->errors());
    }
}
