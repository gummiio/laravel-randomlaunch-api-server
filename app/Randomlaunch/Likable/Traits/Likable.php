<?php

namespace Randomlaunch\Likable\Traits;

use Randomlaunch\Likable\Model\Like;

trait Likable
{

    /**
     * Get all of the model's likables.
     *
     * @return object
     */
    public function likables()
    {
        return $this->morphMany(Like::class, 'likable');
    }

    /**
     * Get all the liked likables
     *
     * @return object
     */
    public function likes()
    {
        return $this->likables()->where('liked', true);
    }

    /**
     * Get all the disliked likables
     *
     * @return object
     */
    public function dislikes()
    {
        return $this->likables()->where('liked', false);
    }

    /**
     * like the current model
     *
     * @return object
     */
    public function like($comment = null)
    {
        $user = Auth()->user();
        return $this->likables()->create([
            'user_id' => $user? $user->getKey() : null,
            'liked' => true,
            'comment' => $comment
        ]);
    }

    /**
     * dislike the current model
     *
     * @return object
     */
    public function dislike($comment = null)
    {
        $user = Auth()->user();
        return $this->likables()->create([
            'user_id' => $user? $user->getKey() : null,
            'liked' => false,
            'comment' => $comment
        ]);
    }
}
