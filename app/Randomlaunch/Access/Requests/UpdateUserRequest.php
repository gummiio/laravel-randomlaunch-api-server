<?php

namespace Randomlaunch\Access\Requests;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;
use Randomlaunch\Access\Model\User;

class UpdateUserRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->is($this->route('users'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'asdf' => 'required'
        ];
    }


}
