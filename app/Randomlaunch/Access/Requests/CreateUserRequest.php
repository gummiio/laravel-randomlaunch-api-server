<?php

namespace Randomlaunch\Access\Requests;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;
use Randomlaunch\Access\Model\User;

class CreateUserRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'asdf' => 'required'
        ];
    }


}
