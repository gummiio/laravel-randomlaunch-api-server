<?php

namespace Randomlaunch\Access\Transformer;

use Illuminate\Database\Eloquent\Model;
use Randomlaunch\Api\Transformer\ApiTransformer;

class UserTransformer extends ApiTransformer
{
    protected $hiddenColumns = ['deleted_at', 'updated_at'];

    protected $transformedKeys = [
        'email_address' => 'email',
        'api_token'     => 'token',
        'created_at'    => 'joined'
    ];
}
