<?php

namespace Randomlaunch\Access\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Randomlaunch\Access\Model\Relationships\UserRelationships;
use Randomlaunch\Access\Transformer\UserTransformer;
use Randomlaunch\Media\Traits\HasMedias;
use Randomlaunch\Model;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract
{
    use Authenticatable, Authorizable, SoftDeletes,
        UserRelationships, HasMedias;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email_address',
        'device_unique_id', 'device_manufacturer', 'device_model',
        'device_id', 'device_name', 'device_locale',
        'system_name', 'system_name', 'system_version'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'last_login'];

    protected $appends = ['auth_key'];

    protected $apiTransformer = UserTransformer::class;

    public function getAuthKeyAttribute()
    {
        return base64_encode(":{$this->api_token}");
    }
}
