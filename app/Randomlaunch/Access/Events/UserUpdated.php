<?php

namespace Randomlaunch\Access\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Randomlaunch\Access\Model\User;

class UserUpdated extends Event
{
    use SerializesModels;

    /**
     * User Object
     *
     * @var object
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @param ApiKey $apiKey ApiKey Object
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
