<?php

namespace Randomlaunch\Access\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Randomlaunch\Access\Model\User;

class UserDeleted extends Event
{
    use SerializesModels;

    /**
     * User id
     *
     * @var object
     */
    public $userID;

    /**
     * Create a new event instance.
     *
     * @param ApiKey $apiKey ApiKey Object
     * @return void
     */
    public function __construct($userID)
    {
        $this->userID = $userID;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
