<?php

namespace App\Listeners\Randomlaunch\Access\Listeners\UserDeleted;

use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Randomlaunch\Access\Events\UserDeleted;
use Randomlaunch\Helpers\SlackHelper as Slack;

class NotifyOnSlack implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserDeleted  $event
     * @return void
     */
    public function handle(UserDeleted $event)
    {
        Slack::new()
            ->text('*A User has deleted his/her account.*')
            ->color('warning')
            ->addField([
                "*User ID*: {$event->userID}",
                "*At*: ".Carbon::now()->toDateTimeString()
            ])
            ->send();
    }
}
