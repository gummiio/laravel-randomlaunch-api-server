<?php

namespace Randomlaunch\Access\Listeners\UserCreated;

use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Randomlaunch\Access\Events\UserCreated;
use Randomlaunch\Helpers\SlackHelper as Slack;

class NotifyOnSlack implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        $user = $event->user;

        Slack::new()
            ->text('*A New Device has Registered.*')
            ->color('good')
            ->addField([
                "*User ID*: {$user->id}",
                "*Device*: `{$user->device_model}` `{$user->device_id}` - {$user->device_name} _({$user->device_locale})_",
                "*System*: `{$user->system_name}` `v{$user->system_version}`",
                "*At*: ".Carbon::now()->toDateTimeString(),
            ])
            ->send();
    }
}
