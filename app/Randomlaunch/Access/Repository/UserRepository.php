<?php

namespace Randomlaunch\Access\Repository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Randomlaunch\Access\Events\UserDeleted;
use Randomlaunch\Access\Events\UserUpdated;
use Randomlaunch\Access\Model\User;
use Randomlaunch\Repository;

class UserRepository extends Repository
{
    public static function getUserFromRequest(Request $request)
    {
        if ($user = User::where('username', $request->username)->first()) {
            if (Hash::check($request->password, $user->password)) {
                return $user;
            }
        }

        return false;
    }

    public static function updateUser(User $user, Request $request)
    {
        $user->update($request->all());

        event(new UserUpdated($user));

        return $user;
    }

    public static function deleteUser(User $user)
    {
        $user->delete();

        event(new UserDeleted($user->id));
    }
}
