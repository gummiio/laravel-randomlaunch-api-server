<?php

namespace Randomlaunch\Reportable;

use Randomlaunch\Reportable\Report;

trait Reportable
{

    /**
     * Get all of the model's reports.
     *
     * @return object
     */
    public function reports()
    {
        return $this->morphMany(Report::class, 'reportable');
    }

    /**
     * report the current model
     *
     * @return object
     */
    public function report($comment = null, $type = null)
    {
        $user = Auth()->user();
        return $this->reports()->create([
            'user_id' => $user? $user->getKey() : null,
            'comment' => $comment,
            'type' => $type,
            'ip' => Request()->ip()
        ]);
    }
}
