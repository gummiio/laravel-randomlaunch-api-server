<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropUnique('users_username_email_address_unique');

            $table->string('email_address')->nullable()->default(null)->change();

            $table->string('device_unique_id')->after('api_token');
            $table->string('device_manufacturer')->nullable()->default(null)->after('device_unique_id');
            $table->string('device_model')->nullable()->default(null)->after('device_manufacturer');
            $table->string('device_id')->nullable()->default(null)->after('device_model');
            $table->string('device_name')->nullable()->default(null)->after('device_id');
            $table->string('device_locale')->nullable()->default(null)->after('device_name');

            $table->string('system_name')->nullable()->default(null)->after('device_locale');
            $table->string('system_version')->nullable()->default(null)->after('system_name');

            $table->unique(['device_unique_id', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropUnique(['device_unique_id', 'deleted_at']);

            $table->dropColumn('device_unique_id');
            $table->dropColumn('device_manufacturer');
            $table->dropColumn('device_model');
            $table->dropColumn('device_id');
            $table->dropColumn('device_name');
            $table->dropColumn('device_locale');

            $table->dropColumn('system_name');
            $table->dropColumn('system_version');

            $table->string('email_address')->change();

            $table->unique(['username','email_address']);
        });
    }
}
