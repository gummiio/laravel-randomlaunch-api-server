<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiScopesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_scopes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('api_scope_group_id')->nullable()->default(null);
            $table->string('name', 64)->unique();
            $table->string('label');
            $table->string('description')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('api_scope_group_id')
                ->references('id')
                ->on('api_scope_groups')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api_scopes');
    }
}
