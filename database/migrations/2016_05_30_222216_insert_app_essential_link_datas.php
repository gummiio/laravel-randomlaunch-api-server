<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Randomlaunch\Link\Model\Category;
use Randomlaunch\Link\Model\Keyword;
use Randomlaunch\Link\Model\Blacklist;

class InsertAppEssentialLinkDatas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();

        $category = Category::create([
            'name' => 'random',
            'label' => 'Random'
        ]);

        factory(Keyword::class)->create([
            'category_id' => $category->id
        ]);

        $regulars = ["youtube","yahoo","google","cnn","bbc","wikipedia","wikimedia","facebook","baidu","live","amazon","qq","twitter","taobao","blogspot","linkedin","sina","msn","mail","googleusercontent","ebay","wordpress","bing","tumblr","weibo","microsoft","pinterest","apple","paypal","blogger","imdb","craigslist","ask","youku","adobe","flickr","instagram","about","stackoverflow","dailymotion","imgur","forums","forum","blogs","blog","hotmail","yandex","netflix","huffingtonpost","redtube","aol","pornhub","porn","xhamster","xvideos","fbi","cia","deviantart","comcast","edu","gov","theme","themes","portfolio","dev","api","developers","quora","dictionary","codepen","jsbin","github","bitbucket", "php"];

        foreach ($regulars as $regular) {
            Blacklist::create([
                'pattern' => $regular,
                'type' => 'regular'
            ]);
        }


        $expressions = ["/porn/","/bitch/","/asshole/","/sh(y|i)t/","/google/","/adobe/", "/dictionar[i|y]/", "/^demo\./"];

        foreach ($expressions as $expression) {
            Blacklist::create([
                'pattern' => $expression,
                'type' => 'regexp'
            ]);
        }

        Model::reguard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
