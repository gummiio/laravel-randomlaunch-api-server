<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('keyword_id')->nullable()->default(null);
            $table->unsignedInteger('category_id')->nullable()->default(null);
            $table->string('domain');
            $table->string('effective_url');
            $table->string('source_url');
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('load_time');
            $table->text('click_stats')->default('');
            $table->unsignedInteger('desktop_screenshot')->nullable()->default(null);
            $table->unsignedInteger('mobile_screenshot')->nullable()->default(null);

            $table->timestamp('launched_at')->nullable();
            $table->timestamp('approved_at')->nullable();
            $table->timestamp('flagged_at')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('keyword_id')
                ->references('id')
                ->on('keywords')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('set null');

            $table->foreign('desktop_screenshot')
                ->references('id')
                ->on('medias')
                ->onDelete('set null');

            $table->foreign('mobile_screenshot')
                ->references('id')
                ->on('medias')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('links');
    }
}
