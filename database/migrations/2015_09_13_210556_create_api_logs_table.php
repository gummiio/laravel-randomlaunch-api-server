<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('api_token')->nullable()->default(null);
            $table->unsignedInteger('api_key_id')->nullable()->default(null);
            $table->unsignedInteger('auth_user_id')->nullable()->default(null);
            $table->string('endpoint');
            $table->string('method');
            $table->text('headers');
            $table->text('params');
            $table->string('ip');
            $table->string('user_agent');
            $table->string('respond_code');
            $table->decimal('respond_time', 16, 6);
            $table->timestamps();

            $table->foreign('api_key_id')
                ->references('id')->on('api_keys')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api_logs');
    }
}
