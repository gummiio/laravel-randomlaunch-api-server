<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 128);
            $table->string('password');
            $table->string('email_address');
            $table->string('api_token', 64)->unique();
            $table->timestamp('last_login')->nullable()->default(null);

            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();

            $table->unique(['username','email_address']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
