<?php

use Illuminate\Database\Seeder;
use Randomlaunch\Access\Model\AdminUser;
use Randomlaunch\Access\Model\User;
use Randomlaunch\Api\Model\ApiKey;
use Randomlaunch\Api\Model\ApiScope;

class GenerateApiAccessDatas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'api_token' => 'asdfasdf',
            'username' => 'gummi',
            'password' => bcrypt('password')
        ]);

        factory(AdminUser::class)->create([
            'username' => 'admin',
            'email_address' => 'admin@localhost.com',
            'api_token' => 'zxcvzxcv'
        ]);

        $apiKey = factory(ApiKey::class)->create([
            'key' => 'asdf'
        ]);

        factory(ApiKey::class)->create([
            'key' => 'zxcv',
            'for_admin' => true
        ]);

        factory(ApiScope::class, 10)->create();

        $apiKey->scopes()->saveMany(ApiScope::orderByRandom()->take(5)->get());
    }
}
