<?php

use Illuminate\Database\Seeder;
use Randomlaunch\Access\Model\User;
use Randomlaunch\Media\Model\Media;
use Randomlaunch\Portfolio\Model\Portfolio;

class GenerateMediaDatas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::first();

        $medias = factory(Media::class, 2)->create([
            'user_id' => $user->id
        ]);
    }
}
