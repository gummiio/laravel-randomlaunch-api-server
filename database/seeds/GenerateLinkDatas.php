<?php

use Illuminate\Database\Seeder;
use Randomlaunch\Link\Model\Category;
use Randomlaunch\Link\Model\Keyword;
use Randomlaunch\Link\Model\Link;

class GenerateLinkDatas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Link::class, 20)->create();
        factory(Category::class, 5)->create();
        factory(Keyword::class, 10)->create();
    }
}
