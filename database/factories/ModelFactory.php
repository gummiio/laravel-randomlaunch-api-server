<?php

use Randomlaunch\Access\Model\AdminUser;
use Randomlaunch\Access\Model\User;
use Randomlaunch\Api\Model\ApiKey;
use Randomlaunch\Api\Model\ApiScope;
use Randomlaunch\Api\Model\ApiScopeGroup;
use Randomlaunch\Likable\Model\Like;
use Randomlaunch\Link\Model\Category;
use Randomlaunch\Link\Model\Keyword;
use Randomlaunch\Link\Model\Link;
use Randomlaunch\Media\Model\Media;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->userName,
        'password' => bcrypt('password'),
        'email_address' => $faker->unique()->safeEmail,
        'remember_token' => str_random(10),
        'api_token' => $faker->unique()->sha256
    ];
});

$factory->define(AdminUser::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->userName,
        'password' => bcrypt('password'),
        'email_address' => $faker->unique()->safeEmail,
        'remember_token' => str_random(10),
    ];
});

$factory->define(ApiKey::class, function (Faker\Generator $faker) {
    return [
        'key' => $faker->unique()->sha256,
        'description' => $faker->bs
    ];
});

$factory->define(ApiScopeGroup::class, function (Faker\Generator $faker) {
    $name = $faker->unique()->sentence(3);

    return [
        'name' => str_slug($name),
        'label' => $name,
        'description' => $faker->sentence
    ];
});

$factory->define(ApiScope::class, function (Faker\Generator $faker) {
    $name = $faker->unique()->sentence(3);

    return [
        'api_scope_group_id' => $faker->boolean(50)? factory(ApiScopeGroup::class)->create()->id : null,
        'name' => str_slug($name),
        'label' => $name,
        'description' => $faker->sentence
    ];
});

$factory->define(Media::class, function (Faker\Generator $faker) {
    return [
        'user_id' => factory(User::class)->create()->id,
        'filename' => $faker->word,
        'extension' => $faker->fileExtension,
        'mime_type' => $faker->mimeType,
        'dir_path' => "/dir/{$faker->word}",
        'url_path' => "www://{$faker->word}",
        'file_size' => $faker->randomNumber,
    ];
});

$factory->define(Like::class, function (Faker\Generator $faker) {
    return [
        'user_id' => factory(User::class)->create()->id,
        'liked' => $faker->boolean,
        'comment' => $faker->sentence
    ];
});

$factory->define(Link::class, function (Faker\Generator $faker) {
    $keyword = factory(Keyword::class)->create();
    $domain = 'http://' . strtolower($faker->unique()->domainName);

    return [
        'keyword_id' => $keyword->id,
        'category_id' => $keyword->category_id,
        'domain' => $domain,
        'effective_url' => $domain,
        'source_url' => $domain . '/' . $faker->slug,
        'title' => $faker->sentence,
        'description' => $faker->sentence,
        'load_time' => $faker->randomFloat(3, 0, 10)
    ];
});

$factory->define(Category::class, function (Faker\Generator $faker) {
    $label = $faker->unique()->sentence(2);
    return [
        'name' => snake_case($label),
        'label' => ucfirst($label),
        'description' => $faker->sentence(10)
    ];
});

$factory->define(Keyword::class, function (Faker\Generator $faker) {
    $label = $faker->unique()->sentence(3);
    return [
        'name' => snake_case($label),
        'label' => ucfirst($label),
        'description' => $faker->sentence(10)
    ];
});
