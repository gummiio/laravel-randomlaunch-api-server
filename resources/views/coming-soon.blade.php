<!DOCTYPE html>
<html>
    <head>
        <title>RandomLaunch</title>

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet" type="text/css">

        <meta name="robots" content="nofollow, noindex" />

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Open Sans';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 52px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">We are currently under construction.</div>
                <p>Please come back soon for the new updates :)</p>
            </div>
        </div>
    </body>
</html>
